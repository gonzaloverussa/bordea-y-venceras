maxDeep(2).


minimax(Role,BestAct):-
	t(control(Role)),
	assert(maxRole(Role)),
    bagof(Act, legal(Role, Act), ActList),
    best(ActList,0,BestAct,_BestScore),
	retract(maxRole(Role)).

%caso base, devuelve la utilidad del camino
getScore(Deep,_Act,Score):-
	(terminal ; maxDeep(Deep)),
	utility(Score).
getScore(Deep,BestAct,BestScore):-
	\+(terminal ; maxDeep(Deep)),
	t(control(Role)),
    bagof(Act, legal(Role, Act), ActList),
    best(ActList,Deep,BestAct,BestScore).
	
best([Act],Deep,Act,Score):-
	t(control(Role)),
	NewDeep is Deep + 1,
	simularJuego(Role,Act),
	getScore(NewDeep,_,Score),
	volverEstadoAnterior.
best([Act|ActList],Deep,BestAct,BestScore):-
	t(control(Role)),
	NewDeep is Deep + 1,
	simularJuego(Role,Act),
	getScore(NewDeep,_,Score),
	volverEstadoAnterior,
	best(ActList,Deep,Act2,Score2),
	betterOf(Act,Score,Act2,Score2,BestAct,BestScore).
	
%agrega un random para cuando el score de 2 caminos es el mismo
betterOf(Act,Score,_Act2,Score,Act,Score):-
	maybe.
betterOf(_Act,Score,Act2,Score,Act2,Score).
%el jugador que tiene el control es el que esta simulando
betterOf(Act,Score,_Act2,Score2,Act,Score):-
	maxRole(Role),
	t(control(Role)),
	Score >= Score2.
betterOf(_Act,Score,Act2,Score2,Act2,Score2):-
	maxRole(Role),
	t(control(Role)),
	Score < Score2.
%el jugador que tiene el control no es el que esta simulando
betterOf(_Act,Score,Act2,Score2,Act2,Score2):-
	maxRole(MaxRole),
	t(control(Role)),
	MaxRole \== Role,
	Score >= Score2.
betterOf(Act,Score,_Act2,Score2,Act,Score):-
	maxRole(MaxRole),
	t(control(Role)),
	MaxRole \== Role,
	Score < Score2.
	
	
utility(Score):-
	maxRole(Role),	%role que esta simulando
	goal(Role,MyScore),
	role(OtherRole),
	OtherRole \== Role,
	goal(OtherRole,OtherScore),
	Score is MyScore - OtherScore.

%%%%%%%%
simularJuego(Role,Act):-
	simularAccion(Role,Act),
	proximo_estado,
	retractall(t(_Y)),
	crea_estado.

simularAccion(Role,Act):-
	retractall(does(_X,_A)),
	assert(does(Role,Act)),
	role(R),
	Role \== R,
	legal(R,A),
	assert(does(R,A)).
	
volverEstadoAnterior:-
	estado(E),
	NewState is E - 1,
	retractall(h(NewState,_A)),
	retract(estado(_)),
	assert(estado(NewState)),
	HistoricoAnterior is NewState - 1,
	retractall(does(_,_)),
	retractall(t(_Y)),
	forall(h(HistoricoAnterior,T),assert(t(T))).
