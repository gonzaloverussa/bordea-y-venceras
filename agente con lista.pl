%:-module(agente, [agente/2]).
%:- use_module(juego, [legal/1,next/1,goal/2,t/1]).
%:-include('Borde').

maxDeep(3).

% minimax(Pos, BestNextPos, Val)
% Pos is a position, Val is its minimax value.
% Best move from Pos leads to position BestNextPos.
%Se tiene movimientos legales y no se llega a la profundidad maxima

agente(Role,BestAct):-
	t(control(Role)),
	assert(maxRole(Role)),
    bagof(Act, legal(Role, Act), ActList),
    best(ActList,0,-9999999999,'',-9999999999,BestAct,_BestScore,'MAX[',Tree),
    %best(ActList,0,BestAct,_BestScore),
	string_concat(Tree,']',Tree2),
	write(Tree2),nl,
	write('bestAct '),write(BestAct),nl,
	retract(maxRole(Role)).

%caso base, devuelve la utilidad del camino
minimax(Deep,_FatherScore,_Act,Score,Tree,Tree2):-
%minimax(Deep,_Act,Score):-
	(terminal ; maxDeep(Deep)),
	utility(Score),
	string_concat(Tree,Score,Tree2).
minimax(Deep,FatherScore,BestAct,BestScore,Tree,Tree4):-
%minimax(Deep,BestAct,BestScore):-
	t(control(Role)),
    bagof(Act, legal(Role, Act), ActList),
	maxRole(MaxRole),	%sacar
	MaxRole \== Role,	%sacar
	string_concat(Tree,'MIN[',Tree2),
    best(ActList,Deep,FatherScore,'',-9999999999,BestAct,BestScore,Tree2,Tree3),
	string_concat(Tree3,']',Tree4).
    %best(ActList,Deep,BestAct,BestScore).
minimax(Deep,FatherScore,BestAct,BestScore,Tree,Tree4):-
%minimax(Deep,BestAct,BestScore):-
	t(control(Role)),
    bagof(Act, legal(Role, Act), ActList),
	maxRole(Role),	%sacar
	string_concat(Tree,'MAX[',Tree2),
    best(ActList,Deep,FatherScore,'',-9999999999,BestAct,BestScore,Tree2,Tree3),
	string_concat(Tree3,']',Tree4).
	
	
%caso en el que se poda
best(ActList,_,FatherScore,CurrentAct,CurrentScore,CurrentAct,CurrentScore,Tree,Tree4):-
	prone(FatherScore,CurrentScore),
	string_concat(Tree,'prone{',Tree2),
	atomic_list_concat(ActList, ',', Atom),
	atom_string(Atom, String),
	string_concat(Tree2,String,Tree3),
	string_concat(Tree3,'}',Tree4).
best([Act],Deep,FatherScore,CurrentAct,CurrentScore,BestAct,BestScore,Tree,Tree5):-
%best([Act],Deep,Act,Score):-
	t(control(Role)),
	NewDeep is Deep + 1,
	simularJuego(Role,Act),
	string_concat(Tree,Act,Tree2),
	string_concat(Tree2,'(',Tree3),
	%write(Tree3),
	minimax(NewDeep,CurrentScore,_,Score,Tree3,Tree4),
	%minimax(NewDeep,_,Score),
	string_concat(Tree4,')',Tree5),
	volverEstadoAnterior,
	betterOf(CurrentAct,CurrentScore,Act,Score,BestAct,BestScore).
best([Act|ActList],Deep,FatherScore,CurrentAct,CurrentScore,BestAct,BestScore,Tree,Tree6):-
%best([Act|ActList],Deep,BestAct,BestScore):-
	t(control(Role)),
	NewDeep is Deep + 1,
	simularJuego(Role,Act),
	string_concat(Tree,Act,Tree2),
	string_concat(Tree2,'(',Tree3),
	%write(Tree3),
	minimax(NewDeep,CurrentScore,_,Score,Tree3,Tree4),
	%minimax(NewDeep,_,Score),
	string_concat(Tree4,'),',Tree5),
	volverEstadoAnterior,
	betterOf(CurrentAct,CurrentScore,Act,Score,NewCurrentAct,NewCurrentScore),
	best(ActList,Deep,FatherScore,NewCurrentAct,NewCurrentScore,BestAct,BestScore,Tree5,Tree6).
	%best(ActList,Deep,Act2,Score2).
	
betterOf(Act,Score,_Act2,Score,Act,Score).
%para cuando todavia no se tiene score
betterOf(_,-9999999999,Act,Score,Act,Score).
%el jugador que tiene el control es el que esta simulando
betterOf(Act,Score,_Act2,Score2,Act,Score):-
	maxRole(Role),
	t(control(Role)),
	Score >= Score2.
betterOf(_Act,Score,Act2,Score2,Act2,Score2):-
	maxRole(Role),
	t(control(Role)),
	Score < Score2.
%el jugador que tiene el control no es el que esta simulando
betterOf(_Act,Score,Act2,Score2,Act2,Score2):-
	maxRole(MaxRole),
	t(control(Role)),
	MaxRole \== Role,
	Score >= Score2.
betterOf(Act,Score,_Act2,Score2,Act,Score):-
	maxRole(MaxRole),
	t(control(Role)),
	MaxRole \== Role,
	Score < Score2.
	
%para cuando el padre todavia no tiene score
prone(-9999999999,_):-
	!,
	fail.
%para cuando el actual todavia no tiene score
prone(_,-9999999999):-
	!,
	fail.
%Caso de Min, si el padre ya tiene un valor mas alto, se poda
prone(Min,BestScore):-
	t(control(Role)),
	\+maxRole(Role),
	Min >= BestScore.
%Caso de Max, si el padre ya tiene un valor mas bajo, se poda
prone(Max,BestScore):-
	t(control(Role)),
	maxRole(Role),
	Max =< BestScore.
	
	
	
utility(Score):-
	maxRole(Role),	%role que esta simulando
	goal(Role,MyScore),
	role(OtherRole),
	OtherRole \== Role,
	goal(OtherRole,OtherScore),
	Score is MyScore - OtherScore.

%%%%%%%%
simularJuego(Role,Act):-
	simularAccion(Role,Act),
	proximo_estado,
	retractall(t(_Y)),
	crea_estado.

simularAccion(Role,Act):-
	retractall(does(_X,_A)),
	assert(does(Role,Act)),
	role(R),
	Role \== R,
	legal(R,A),
	assert(does(R,A)).
	
volverEstadoAnterior:-
	estado(E),
	NewState is E - 1,
	retractall(h(NewState,_A)),
	retract(estado(_)),
	assert(estado(NewState)),
	HistoricoAnterior is NewState - 1,
	retractall(does(_,_)),
	retractall(t(_Y)),
	forall(h(HistoricoAnterior,T),assert(t(T))).
