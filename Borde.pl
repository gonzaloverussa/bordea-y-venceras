%roles azul y rojo
role(o).
role(x).

%marca del owner
owner(o,0).
owner(x,*).

%posiciones iniciales de los roles
initPos(o,1,Col):-
	cols(Cols),
	middlePoint(1,Cols,Col).
initPos(x,Rows,Col):-
	rows(Rows),
	cols(Cols),
	middlePoint(1,Cols,Col).

%role inicial
initRole(x).

%cantidad de filas y columnas
rows(9).
cols(9).

%puntaje con el que se gana
goalScore(Score):-
	rows(Rows),
	cols(Cols),
	Score is Rows * Cols * 5.

%define dinamicamente los indexX segun cuantas rows hay
defineIndexX:-
	rows(Rows),
	between(1,Rows,X),
	assert(indexX(X)),
	fail.
defineIndexX.

%define dinamicamente los indexY segun cuantas cols hay
defineIndexY:-
	cols(Cols),
	between(1,Cols,Y),
	assert(indexY(Y)),
	fail.
defineIndexY.

middlePoint(X,X1,Average):- 
    Sum is X + X1,
    Average is truncate(Sum / 2).
	
%estado inicial
init(cell(X,Y,R)):-initPos(R,X,Y),indexXY(X,Y).
init(cell(X,Y,b)):-indexXY(X,Y),\+initPos(_,X,Y).
init(control(R)):-initRole(R).
init(score(R,0)):-role(R).


%posibles valores que pueden tener las relaciones
base(control(X)):- role(X).
base(cell(X,Y,b)):- indexXY(X,Y).
base(cell(X,Y,R)):- role(R),indexXY(X,Y).
base(cell(X,Y,O)):- owner(_,O),indexXY(X,Y).

%posibles valores que pueden tener las entradas
input(R,noop):-role(R).
input(R,up):-role(R).
input(R,down):-role(R).
input(R,left):-role(R).
input(R,right):-role(R).

%movimientos legales
legal(R,noop):-
	role(R),	%se pone para que instancie R
	\+t(control(R)).
legal(R,up):-
	t(control(R)),
	t(cell(Row,Col,R)),
	NewRow is Row - 1,
	legalMove(NewRow,Col).
legal(R,down):-
	t(control(R)),
	t(cell(Row,Col,R)),
	NewRow is Row + 1,
	legalMove(NewRow,Col).
legal(R,left):-
	t(control(R)),
	t(cell(Row,Col,R)),
	NewCol is Col - 1,
	legalMove(Row,NewCol).
legal(R,right):-
	t(control(R)),
	t(cell(Row,Col,R)),
	NewCol is Col + 1,
	legalMove(Row,NewCol).
	
legalMove(X,Y):-	%verifica que el movimiento no lo deje donde esta otro jugador
	indexXY(X,Y),
	t(cell(X,Y,R)),
	\+role(R).

%próximo estado
next(_):-
	t(control(R)),
	searchFigure(R),
	fail.
	
%se asigna el role a la siguiente celda
next(cell(NewX,NewY,R)):-
	does(R,Act),
	t(cell(X,Y,R)),
	move(X,Y,Act,NewX,NewY).

%marca la casilla que dejo el role
next(cell(X,Y,Own)):-
	does(R,Act),
	changePos(Act),
	t(cell(X,Y,R)),
	owner(R,Own).
	
%marca las celdas que estan dentro del perimetro
next(cell(X,Y,Own)):-
	t(cell(X,Y,C)),
	\+role(C),
	isInArea(X,Y),
	t(control(R)),
	owner(R,Own).
	
%mantiene las celdas como estaban, debe ir luego de calcular todas las
% emás celdas para no generar mas de un cell por celda
next(cell(X,Y,M)):-
	t(cell(X,Y,M)),
	estado(E),
	\+h(E,cell(X,Y,_)).	%%modificar. NO SE DEBE USAR EL H
	
%cambia el control
next(control(R)):-
	role(R),
	\+t(control(R)).
	
%calcula el nuevo score de cada role en los estados par
next(score(R,NewScore)):-
	estado(E),
	0 is mod(E,2),
	%initRole(R),
	t(score(R,Score)),
	owner(R,O),
	aggregate_all(count, h(E,cell(_,_,O)), ScoreToAdd),
	NewScore is Score + ScoreToAdd + 1.
	
next(score(R,Score)):-
	estado(E),
	1 is mod(E,2),
	t(score(R,Score)).

%borra el perimetro calculado
next(_):-
	retractall(perimeter(_,_)),
	!,
	fail.
	
%es verdadero para todas las acciones que cambian la posicion del role
changePos(Act):-Act \== noop.

%devuelve la nueva posicion del role al hacer up, down, left o right
move(X,Y,up,NewX,Y):- NewX is X - 1.
move(X,Y,down,NewX,Y):- NewX is X + 1.
move(X,Y,left,X,NewY):- NewY is Y - 1.
move(X,Y,right,X,NewY):- NewY is Y + 1.
move(X,Y,noop,X,Y).

%%%%%%%%%%%%%%%%%%%%%%%%%% Busca figuras %%%%%%%%%%%%%%%%%%%%%%%%%%
searchFigure(R):-
	nextPos(R,X,Y),
	nextCellOfRole(X,Y,[],R,NextX,NextY),
	searchFigureAux(X,Y,R,NextX,NextY,[]),
	retractall(failedCell(_,_)).
searchFigure(_):-
	retractall(failedCell(_,_)),
	fail.

%auxiliar para que la primer celda no vuelva directamente al Init
searchFigureAux(InitX,InitY,R,X,Y,Perimeter):-
	nextCellOfRole(X,Y,Perimeter,R,NextX,NextY),
	(InitX \== NextX ; InitY \== NextY),
	searchInit(InitX,InitY,R,NextX,NextY,[(X,Y)|Perimeter]).

%se llego al punto inicial
searchInit(InitX,InitY,_,InitX,InitY,Perimeter):-
	forall(
		member((X,Y),Perimeter),
		assert(perimeter(X,Y))),
	assert(perimeter(InitX,InitY)),
	fail.
%se busca el siguiente punto adyacente que sea del mismo dueño
searchInit(InitX,InitY,R,X,Y,Perimeter):-
	nextCellOfRole(X,Y,Perimeter,R,NextX,NextY),
	searchInit(InitX,InitY,R,NextX,NextY,[(X,Y)|Perimeter]).
%caso de fallo, no hay celda adyacente que controle R

	
%todas las celdas adyacentes que sean del role R y 
%sin devolver la anterior ni las fuera del tablero
nextCellOfRole(X,Y,Perimeter,R,NextX,NextY):-
	nearCell(X,Y,NextX,NextY),
	indexXY(NextX,NextY),
	\+member((NextX,NextY),Perimeter),
	\+failedCell(NextX,NextY),
	owner(R,O),
	%R esta en la celda, es duenio de la celda o R va a estar en el siguiente turno en la celda
	(t(cell(NextX,NextY,R)) ; t(cell(NextX,NextY,O)) ; nextPos(R,NextX,NextY)),
	\+isInArea(NextX,NextY).
nextCellOfRole(X,Y,_,_,_,_):-
	assert(failedCell(X,Y)),
	fail.

%celdas adyacentes
nearCell(X,Y,NextX,Y):- NextX is X + 1.	%down
nearCell(X,Y,NextX,Y):- NextX is X - 1.	%up
nearCell(X,Y,X,NextY):- NextY is Y + 1.	%right
nearCell(X,Y,X,NextY):- NextY is Y - 1.	%left
	
%verifica si la celda esta dentro de la figura
isInArea(X,Y):-
	perimeter(X1,Y),
	X1 > X,
	perimeter(X2,Y),
	X2 < X,
	perimeter(X,Y1),
	Y1 > Y,
	perimeter(X,Y2),
	Y2 < Y.
	
nextPos(R,NextX,NextY):-
	does(R,Act),
	t(cell(PreX,PreY,R)),
	move(PreX,PreY,Act,NextX,NextY).
	
%verifica si el punto esta dentro de la matriz
indexXY(X,Y):-
	indexX(X),
	indexY(Y).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
goal(R,Score):-role(R),t(score(R,Score)).

terminal:-
	goalScore(GoalScore),
	t(score(_,Score)),
	Score >= GoalScore.
	
distinct(X,Y):-
	X \== Y.

%inicializa estado inicial y borra historial
:-dynamic t/1,h/2,estado/1,does/2,indexX/1,indexY/1,perimeter/2,failedCell/2,up/0,down/0,right/0,left/0,estadoSimulado/1,maxRole/1.
inicio:-
	%make,	%para no tener que recargar el archivo al modificar
	retractall(t(_)),
	retractall(h(_,_)),
	retractall(estado(_)),
	retractall(indexX(_)),
	retractall(indexY(_)),
	retractall(does(_,_)),
	retractall(failedCell(_,_)),
	retractall(maxRole(_)),
	defineIndexX,
	defineIndexY,
	crea_estado_inicial,
	imprime.

%crea estado inicial
crea_estado_inicial:-
	init(X),
    \+(t(X)),
	assert(t(X)),
    assert(h(0,X)),
	crea_estado_inicial.
crea_estado_inicial:-
	assert(estado(1)).


% gestor del juego
% borra acciones viejas
% busca nuevas acciones
% calcula próximo estado
% crea proximo estado

juego:-
	\+terminal,
	retractall(does(_X,_A)),
	inserta_acciones,
	proximo_estado,
	retractall(t(_Y)),
	crea_estado,
	imprime,
	juego.
juego:-
	terminal,
	forall(role(R), ((goal(R,S)),
					write(R),
					write(' consiguio '),
					write(S),
					write(' puntos'),
					nl)).
	%goal(x,Px),goal(o,Po),
	%write('x gano '),write(Px),write(' puntos y o gano '),
	%write(Po),write(' puntos.').

% busca las nuevas acciones de los jugadores y las inserta
inserta_acciones:-
	t(control(X)),
	jugador(X,A),
	legal(X,A),
	assert(does(X,A)),
	role(O),
	X \== O,
	assert(does(O,noop)).

%calcula el próximo estado
proximo_estado:-
	estado(E),
	forall((next(Y), \+h(E,Y)), assert(h(E,Y))).

%crea el estado actual
crea_estado:-
	estado(E),
	forall((h(E,Y), \+t(Y)), assert(t(Y))),
	retract(estado(State)),
	NewState is State +1,
	assert(estado(NewState)).
	
%imprime estado actual del juego
imprime:-
	%write('\e[2J'),	%limpia la consola
	estado(E),
	write('Estado: '),write(E),nl,
	imprimeScore,
	t(control(X)),
	write('Control: '),write(X),nl,
	imprimeTablero(1),
	write('*****************'),nl.

imprimeScore:-
	forall(role(R),(write('Puntaje del jugador '),
					write(R),
					write(': '),
					t(score(R,Score)),
					write(Score),
					nl)
			).

%%%%%%%%%%%%%%%%% Imprime el tablero%%%%%%%%%%%%%%
imprimeTablero(Row):-
	rows(MaxRow),
	Row > MaxRow.
imprimeTablero(Row):-
	rows(MaxRow),
	Row =< MaxRow,
	imprimeFila(Row,1),
	Row1 is Row + 1,
	imprimeTablero(Row1).

imprimeFila(_,Col):-
	cols(MaxCol),
	Col > MaxCol,
	nl.
imprimeFila(Row,Col):-
	cols(MaxCol),
	Col =< MaxCol,
	imprimeCelda(Row,Col),
	Col1 is Col + 1,
	imprimeFila(Row,Col1).

imprimeCelda(Row,Col):-
	t(cell(Row,Col,R)),
	owner(R,_),
	imprimeRole(R),
	write(' ').
imprimeCelda(Row,Col):-
	t(cell(Row,Col,O)),
	owner(_,O),
	imprimeOwner(O),
	write(' ').
imprimeCelda(Row,Col):-
	t(cell(Row,Col,_)),
	imprimeBlanco,
	write(' ').

%simbolo que represnta al role
imprimeRole(x):-ansi_format([fg(red)],'\u25B2',[]).
imprimeRole(o):-ansi_format([fg(blue)],'\u25B2',[]).

%simbolo que represnta la casillas que controla el role
imprimeOwner(O):-owner(x,O),ansi_format([fg(red)],'\u25A0',[]).
imprimeOwner(O):-owner(o,O),ansi_format([fg(blue)],'\u25A0',[]).

%simbolo que representa la casilla en blanco
imprimeBlanco:-ansi_format([fg(black)],'\u25A1',[]).
   
/*
%desarrollo jugador x
jugador(x,X):-
    bagof(Act, legal(x, Act), ActList),
	write('Su acciones legales son: '),write(ActList),nl,
	write('Ingrese proximo movimiento: '),
	read(X).
jugador(x,X):-
	write('Accion no legal'),nl,
	jugador(x,X).
*/
/*
%agente que toma acciones random
jugador(R,Act):-
	findall(X,legal(R,X),ActList),
	length(ActList, Length),
	random(0, Length, Random),
	nth0(Random,ActList,Act).*/

%agente con minimax
:-include('poda').
%:-include('minimax').
jugador(R,Act):-
	poda(R,Act).
%:-include('agente2').
%jugador(R,Act):-
%	minimax(R,Act).

?- inicio,juego.
