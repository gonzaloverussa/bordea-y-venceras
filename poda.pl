maxDeep(1).


poda(Role,BestAct):-
	t(control(Role)),
	assert(maxRole(Role)),
    bagof(Act, legal(Role, Act), ActList),
    best(ActList,0,-9999999999,'',-9999999999,BestAct,_BestScore),
	retract(maxRole(Role)).

%caso base, devuelve la utilidad del camino
getScore(Deep,_FatherScore,_Act,Score):-
	(terminal ; maxDeep(Deep)),
	utility(Score).
getScore(Deep,FatherScore,BestAct,BestScore):-
	\+(terminal ; maxDeep(Deep)),
	t(control(Role)),
    bagof(Act, legal(Role, Act), ActList),
    best(ActList,Deep,FatherScore,'',-9999999999,BestAct,BestScore).
	
	
%caso en el que se poda
best(_ActList,_,FatherScore,CurrentAct,CurrentScore,CurrentAct,CurrentScore):-
	prone(FatherScore,CurrentScore).
best([Act],Deep,_FatherScore,CurrentAct,CurrentScore,BestAct,BestScore):-
	t(control(Role)),
	NewDeep is Deep + 1,
	simularJuego(Role,Act),
	getScore(NewDeep,CurrentScore,_,Score),
	volverEstadoAnterior,
	betterOf(CurrentAct,CurrentScore,Act,Score,BestAct,BestScore).
best([Act|ActList],Deep,FatherScore,CurrentAct,CurrentScore,BestAct,BestScore):-
	t(control(Role)),
	NewDeep is Deep + 1,
	simularJuego(Role,Act),
	getScore(NewDeep,CurrentScore,_,Score),
	volverEstadoAnterior,
	betterOf(CurrentAct,CurrentScore,Act,Score,NewCurrentAct,NewCurrentScore),
	best(ActList,Deep,FatherScore,NewCurrentAct,NewCurrentScore,BestAct,BestScore).
	
%agrega un random para cuando el score de 2 caminos es el mismo
betterOf(Act,Score,_Act2,Score,Act,Score):-
	maybe.
betterOf(_Act,Score,Act2,Score,Act2,Score).
%para cuando todavia no se tiene score
betterOf(_,-9999999999,Act,Score,Act,Score).
%el jugador que tiene el control es el que esta simulando
betterOf(Act,Score,_Act2,Score2,Act,Score):-
	maxRole(Role),
	t(control(Role)),
	Score >= Score2.
betterOf(_Act,Score,Act2,Score2,Act2,Score2):-
	maxRole(Role),
	t(control(Role)),
	Score < Score2.
%el jugador que tiene el control no es el que esta simulando
betterOf(_Act,Score,Act2,Score2,Act2,Score2):-
	maxRole(MaxRole),
	t(control(Role)),
	MaxRole \== Role,
	Score >= Score2.
betterOf(Act,Score,_Act2,Score2,Act,Score):-
	maxRole(MaxRole),
	t(control(Role)),
	MaxRole \== Role,
	Score < Score2.
	
%para cuando el padre todavia no tiene score
prone(-9999999999,_):-
	!,
	fail.
%para cuando el actual todavia no tiene score
prone(_,-9999999999):-
	!,
	fail.
%Caso de Min, si el padre ya tiene un valor mas alto, se poda
prone(Min,BestScore):-
	t(control(Role)),
	\+maxRole(Role),
	Min >= BestScore.
%Caso de Max, si el padre ya tiene un valor mas bajo, se poda
prone(Max,BestScore):-
	t(control(Role)),
	maxRole(Role),
	Max =< BestScore.
	
	
utility(Score):-
	maxRole(Role),	%role que esta simulando
	goal(Role,MyScore),
	role(OtherRole),
	OtherRole \== Role,
	goal(OtherRole,OtherScore),
	Score is MyScore - OtherScore.

%%%%%%%%
simularJuego(Role,Act):-
	simularAccion(Role,Act),
	proximo_estado,
	retractall(t(_Y)),
	crea_estado.

simularAccion(Role,Act):-
	retractall(does(_X,_A)),
	assert(does(Role,Act)),
	role(R),
	Role \== R,
	legal(R,A),
	assert(does(R,A)).
	
volverEstadoAnterior:-
	estado(E),
	NewState is E - 1,
	retractall(h(NewState,_A)),
	retract(estado(_)),
	assert(estado(NewState)),
	HistoricoAnterior is NewState - 1,
	retractall(does(_,_)),
	retractall(t(_Y)),
	forall(h(HistoricoAnterior,T),assert(t(T))).
